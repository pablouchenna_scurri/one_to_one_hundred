'''

@author: Pablo
'''

from __future__ import print_function  # python 2.7 compatible


def print_one_to_one_hundred_ideomatic():
    '''
    Prints the numbers from 1 to 100.
    But for multiples of three print “Three” instead of the number
    and for the multiples of five print “Five”.
    For numbers which are multiples of both three and five print “ThreeFive”.

    Implemented in a simple idiomatic way.
    '''
    print(*['threefive' if (not (i % 3) and not (i % 5))
            else 'three' if not (i % 3)
            else 'five' if not (i % 5)
            else i
            for i in range(1, 101)],
          sep='\n')  # os.linesep not used, explanation: https://docs.python.org/release/3.2/library/os.html#os.linesep


def main():
    print_one_to_one_hundred_ideomatic()


if __name__ == "__main__":
    main()
