'''

@author: Pablo
'''

from __future__ import print_function
import math
import inflect

inflect_engine = inflect.engine()
number_to_words = inflect_engine.number_to_words


def get_iteration_template(length, factors):
    '''
    Generate a multiline str template with place holders numbered
    from 1 to length (one per line),
    but for multiples of any of factors, the factor number is spelled instead.

    Example get_iteration_template(100, (3, 5)):
      Generates a multiline str with place holders numbered from 0 to (100 - 1).
      But for multiples of three puts “Three” instead of the number
      and for the multiples of five puts “Five”.
      For numbers which are multiples of both three and five puts “ThreeFive”.
      """
      {0[0}
      {0[1}
      Three
      {0[2]}
      Five
      ...
      ...
      """
    :param length:
    :param factors:
    '''
    template = ""
    for i in range(1, length + 1):
        add_number = True
        for factor in factors:
            if not(i % factor):
                template += number_to_words(factor)
                add_number = False
        if add_number:
            template += '{{0[{0}]}}'.format(i - 1)
        template += '\n'
    return template.removesuffix('\n')


def print_iteration(iteration_numbers, template, limit=None):
    '''
    Prints template.format(iteration_numbers)
    limiting the number of lines printed to limit if provided.

    :param iteration_numbers: numbers that will populate the template to be printed
    :param template: the str template populated with iteration_numbers
    :param limit: limits number of lines the printed
    '''
    iteration_str = template.format(iteration_numbers)
    if limit is None:
        print(iteration_str)
    elif limit > 0:
        template_lines = iteration_str.split('\n', limit)
        print('\n'.join(template_lines[:-1]))
    else:
        print()


def print_one_to_one_hundred(maximum=100, factors=(3, 5)):
    '''
    Prints the numbers from 1 to maximum.
    But for multiples of any of factors, 
    the factor number is printed spelled instead.

    Example print_one_to_one_hundred(maximum=100, factors=(3, 5)):
      Prints the numbers from 1 to 100. 
      But for multiples of three prints “Three” instead of the number
      and for the multiples of five prints “Five”.
      For numbers which are multiples of both three and five prints “ThreeFive”.
      """
      1
      2
      three
      4
      five
      three
      """

    :param maximum: It will print from 1 to maximum. It should be >= 1
    :param factors: more that two factors can be used. Example: (3, 5, 7)
    '''
    iteration_printer = print_iteration
    less_common_multiplier = math.lcm(*factors)
    template_generator = get_iteration_template
    template_iteration = template_generator(less_common_multiplier, factors)

    quotient, remainder = divmod(maximum, less_common_multiplier)
    iteration_numbers = [x for x in range(1, less_common_multiplier + 1)]
    for i in range(1, quotient + 1):
        iteration_printer(iteration_numbers, template_iteration)
        iteration_numbers = [*map(lambda x: x + less_common_multiplier,
                                  iteration_numbers)]
    if remainder:
        iteration_printer(iteration_numbers, template_iteration, limit=remainder)


def main():
    print_one_to_one_hundred()


if __name__ == "__main__":
    main()
