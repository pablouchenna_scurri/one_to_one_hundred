'''
@author: pablo
'''

from setuptools import setup, find_packages


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='one_to_one_hundred',
      version='1.0.0',
      description='''Prints the numbers from 1 to 100.
But for multiples of three print “Three” instead of the number
and for the multiples of five print “Five”.
For numbers which are multiples of both three and five print “ThreeFive”.''',
      long_description=readme(),
      author='Pablo Uchenna',
      author_email='pablouchennaonuohacid@gmail.com',
      license='LICENSE.txt',
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Intended Audience :: Scurri',
          'Topic :: Programming Exercise',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3.9',
      ],
      packages=find_packages(),
      install_requires=['inflect==5.3.0',
      ],
      python_requires='>=3.9',
      test_suite='tests',
      entry_points={
          'console_scripts': ['one_to_one_hundred=one_to_one_hundred.one_to_one_hundred:main',
                              'one_to_one_hundred_ideomatic=one_to_one_hundred.one_to_one_hundred_ideomatic:main'],
      },
      zip_safe=False)
