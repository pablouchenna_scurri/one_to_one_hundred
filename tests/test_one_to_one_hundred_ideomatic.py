'''
@author: Pablo

python -m unittest discover
'''
from contextlib import contextmanager
from io import StringIO
import os
import sys
import unittest

# needed to run the tests from any directory to import one_to_one_hundred
this_file_dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(this_file_dir_path, '..')) # append parent directory
sys.path.append(os.path.join(this_file_dir_path, '.')) # append current directory

from one_to_one_hundred import one_to_one_hundred_ideomatic

from data import long_output

@contextmanager
def captured_output():
    '''
    Helper to test sys.stdout and sys.stderr
    '''
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err


class Test(unittest.TestCase):

    def setUp(self):
        self.maxDiff = 1000000
        self.expected_output = long_output.ONE_TO_ONE_HUNDRED_EXPECTED_OUTPUT

    def tearDown(self):
        pass

    def test_print_one_to_one_hundred_ideomatic(self):
        with captured_output() as (out, err):
            one_to_one_hundred_ideomatic.print_one_to_one_hundred_ideomatic()
        output = out.getvalue().strip()
        self.assertMultiLineEqual(output, 
                         self.expected_output, 
                         'Unexpected output.\n' + 
                         ('stderror:\n' + err.getvalue().strip()) if err.getvalue() 
                         else '')


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testone_to_one_hundred']
    unittest.main()
