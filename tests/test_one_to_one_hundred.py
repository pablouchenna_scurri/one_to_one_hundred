'''
@author: Pablo

python -m unittest discover
'''
from contextlib import contextmanager
from io import StringIO
import os
import sys
import unittest

# needed to run the tests from any directory to import one_to_one_hundred
this_file_dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(this_file_dir_path, '..')) # append parent directory
sys.path.append(os.path.join(this_file_dir_path, '.')) # append current directory

from one_to_one_hundred import one_to_one_hundred

from data import long_output

@contextmanager
def captured_output():
    '''
    Helper to test sys.stdout and sys.stderr
    '''
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err


class Test(unittest.TestCase):

    def setUp(self):
        self.maxDiff = 1000000
        self.expected_output = long_output.ONE_TO_ONE_HUNDRED_EXPECTED_OUTPUT

    def tearDown(self):
        pass

    def test_get_iteration_template(self):
        expected_output = '''{0[0]}
{0[1]}
three
{0[3]}
five
three
{0[6]}
{0[7]}
three
five
{0[10]}
three
{0[12]}
{0[13]}
threefive'''
        self.assertMultiLineEqual(one_to_one_hundred.get_iteration_template(15, (3, 5)), 
                         expected_output, 
                         'Unexpected output.\n')

    def test_get_iteration_template_0(self):
        expected_output = ''
        self.assertMultiLineEqual(one_to_one_hundred.get_iteration_template(0, (3, 5)), 
                         expected_output, 
                         'Unexpected output.\n')

    def test_get_iteration_template_empty_factors(self):
        expected_output = '''{0[0]}
{0[1]}
{0[2]}
{0[3]}
{0[4]}
{0[5]}
{0[6]}
{0[7]}
{0[8]}
{0[9]}
{0[10]}
{0[11]}
{0[12]}
{0[13]}
{0[14]}'''
        self.assertMultiLineEqual(one_to_one_hundred.get_iteration_template(15, []), 
                         expected_output, 
                         'Unexpected output.\n')

    def test_print_iteration(self):
        iteration_numbers = [x for x in range(1, 15 + 1)]
        template_iteration = '''{0[0]}
{0[1]}
three
{0[3]}
five
three
{0[6]}
{0[7]}
three
five
{0[10]}
three
{0[12]}
{0[13]}
threefive'''
        with captured_output() as (out, err):
            one_to_one_hundred.print_iteration(iteration_numbers, template_iteration)
        output = out.getvalue().strip()
        expected_output = '''1
2
three
4
five
three
7
8
three
five
11
three
13
14
threefive'''
        self.assertMultiLineEqual(output, 
                         expected_output, 
                         'Unexpected output.\n' + 
                         ('stderror:\n' + err.getvalue().strip()) if err.getvalue() 
                         else '')

    def test_print_iteration_with_limit_to_0(self):
        iteration_numbers = [x for x in range(1, 15 + 1)]
        template_iteration = '''{0[0]}
{0[1]}
three
{0[3]}
five
three
{0[6]}
{0[7]}
three
five
{0[10]}
three
{0[12]}
{0[13]}
threefive'''
        with captured_output() as (out, err):
            one_to_one_hundred.print_iteration(iteration_numbers, template_iteration, limit=0)
        output = out.getvalue().strip()
        expected_output = ''
        self.assertMultiLineEqual(output, 
                         expected_output, 
                         'Unexpected output.\n' + 
                         ('stderror:\n' + err.getvalue().strip()) if err.getvalue() 
                         else '')

    def test_print_iteration_with_limit_to_10(self):
        iteration_numbers = [x for x in range(1, 15 + 1)]
        template_iteration = '''{0[0]}
{0[1]}
three
{0[3]}
five
three
{0[6]}
{0[7]}
three
five
{0[10]}
three
{0[12]}
{0[13]}
threefive'''
        with captured_output() as (out, err):
            one_to_one_hundred.print_iteration(iteration_numbers, template_iteration, limit=10)
        output = out.getvalue().strip()
        expected_output = '''1
2
three
4
five
three
7
8
three
five'''
        self.assertMultiLineEqual(output, 
                         expected_output, 
                         'Unexpected output.\n' + 
                         ('stderror:\n' + err.getvalue().strip()) if err.getvalue() 
                         else '')

    def test_print_one_to_one_hundred_default(self):
        with captured_output() as (out, err):
            one_to_one_hundred.print_one_to_one_hundred()
        output = out.getvalue().strip()
        expected_output = self.expected_output
        self.assertMultiLineEqual(output, 
                         expected_output, 
                         'Unexpected output.\n' + 
                         ('stderror:\n' + err.getvalue().strip()) if err.getvalue() 
                         else '')

    def test_print_one_to_one_hundred_0(self):
        with captured_output() as (out, err):
            one_to_one_hundred.print_one_to_one_hundred(0)
        output = out.getvalue().strip()
        expected_output = ''
        self.assertMultiLineEqual(output, 
                         expected_output, 
                         'Unexpected output.\n' + 
                         ('stderror:\n' + err.getvalue().strip()) if err.getvalue() 
                         else '')

    def test_print_one_to_one_hundred_3(self):
        with captured_output() as (out, err):
            one_to_one_hundred.print_one_to_one_hundred(3)
        output = out.getvalue().strip()
        expected_output = '1\n2\nthree'
        self.assertMultiLineEqual(output, 
                         expected_output, 
                         'Unexpected output.\n' + 
                         ('stderror:\n' + err.getvalue().strip()) if err.getvalue() 
                         else '')

    def test_print_one_to_one_hundred_15(self):
        with captured_output() as (out, err):
            one_to_one_hundred.print_one_to_one_hundred(15)
        output = out.getvalue().strip()
        expected_output = '''1
2
three
4
five
three
7
8
three
five
11
three
13
14
threefive'''
        self.assertMultiLineEqual(output, 
                         expected_output, 
                         'Unexpected output.\n' + 
                         ('stderror:\n' + err.getvalue().strip()) if err.getvalue() 
                         else '')


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testone_to_one_hundred']
    unittest.main()
