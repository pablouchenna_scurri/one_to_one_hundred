One to one hundred
------------------
::

    Prints the numbers from 1 to 100. 
    But for multiples of three print “Three” instead of the number
    and for the multiples of five print “Five”.
    For numbers which are multiples of both three and five print “ThreeFive”.


Some possible solutions:
-------------------------------------------------------------------------------------------------------------

* Fixed string or tuple or DB entry, etc

  - The quickest. No calculations required.
  - Hard to scale the range with memory limitations.
  - Hard to scale/change the exceptions.
  - Very easy to test.
  

* **(Implemented)** Loop with if: print_one_to_one_hundred_ideomatic()

  - Ideomatic
  - The slowest. More calculations required for the ifs "multiple of three, five". A counter could be used instead.
  - Easy to scale the range.
  - Easy to scale/change the exceptions.
  - Easy to test.


* **(Implemented)** Loop with Template "Least common multiple" print_one_to_one_hundred(maximum=100, factors=(3, 5))

  - Slow. Some calculations required, like the "Least common multiple"
  - Ready to scale the range.
  - Ready to scale/change the exceptions.
  - A bit more complex to test.


* 1 to 15 multiples fixed tuple

  - Quick. Few calculations required, only an addition for the non multiples numbers, like 2 + 15 or 2 + 30
  - Easy to scale the range.
  - Hard to scale/change the exceptions.
  - Easy to test.
  - It may use a function like:
    ::

        def print_15_period(accumulator=0)
            print(1 + accumulator)
            print(2 + accumulator)
            print('three')
            print(4 + accumulator)
            print('five')
            print('three')
            print(7 + accumulator)
            print(8 + accumulator)
            print('three')
            print('five')
            print(11 + accumulator)
            print(three)
            print(13 + accumulator)
            print(14 + accumulator)
            print('threefive')



Installation
------------

It requires python >=3.9

Go to the installation directory of you choice.

To work in a virtual environment you may use:
::

$ pip install --user pipenv
$ pipenv --python 3.9
$ pipenv install
$ pipenv shell  # activates the virtual environment


Use
----

From the root directory:
::

$ cd one_to_one_hundred

::

$ python one_to_one_hundred.py

Alternative implementation:
::

$ python one_to_one_hundred_ideomatic.py


Tests
-----

From the root directory:
::

$ python setup.py test


Version
-------

BumpVersion can be used to change the version wherever it is needed


Issues
--------

*Just if you want to build a python package*
	For different reasons I had to install the following and then recomplile python 3.9:
	::
	
	  sudo apt install libffi-dev -y
	  sudo apt-get install libssl-dev
	  sudo apt-get install zlib1g-dev
	  sudo apt install libsqlite3-dev
	
	And then recomplile python 3.9:
	::
	
	  make clean
	  ./configure
	  make
	  make test
	  sudo make altinstall


Comments
--------

Regards,
Pablo
